package com.demo.demoapp.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import com.demo.demoapp.Connections.RetrofitConn;
import com.demo.demoapp.Model.AddressInfo;
import com.demo.demoapp.Presenter.MapContract;
import com.demo.demoapp.Presenter.MapPresenter;
import com.demo.demoapp.R;
import com.demo.demoapp.UserConfig;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hms.common.ApiException;
import com.huawei.hms.common.ResolvableApiException;
import com.huawei.hms.location.FusedLocationProviderClient;
import com.huawei.hms.location.LocationAvailability;
import com.huawei.hms.location.LocationCallback;
import com.huawei.hms.location.LocationRequest;
import com.huawei.hms.location.LocationResult;
import com.huawei.hms.location.LocationServices;
import com.huawei.hms.location.LocationSettingsRequest;
import com.huawei.hms.location.LocationSettingsResponse;
import com.huawei.hms.location.LocationSettingsStatusCodes;
import com.huawei.hms.location.SettingsClient;
import com.huawei.hms.maps.CameraUpdate;
import com.huawei.hms.maps.CameraUpdateFactory;
import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.MapView;
import com.huawei.hms.maps.OnMapReadyCallback;
import com.huawei.hms.maps.model.BitmapDescriptorFactory;
import com.huawei.hms.maps.model.CameraPosition;
import com.huawei.hms.maps.model.LatLng;
import com.huawei.hms.maps.model.Marker;
import com.huawei.hms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * map activity entrance class
 */
public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, MapContract.HandlingView {

    MapContract.HandlingPresenter mapPresenter;
    private static final String TAG = "MapViewDemoActivity";
    //Huawei map
    private HuaweiMap hMap;
    private MapView mMapView;
    private CardView getLocation;
    private Marker currentLocationMarker;
    private Marker pinMarker;
    private LatLng currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init view
        mMapView = findViewById(R.id.mapView);
        getLocation = findViewById(R.id.getLocation);

        // on get location button click (get current location)
        getLocation.setOnClickListener(click -> {
            if (currentLocation != null) {
                changeCameraPosation(currentLocation);
            } else {
                mapPresenter.initLocationKit();
            }
        });
        // init map presenter
        mapPresenter = new MapPresenter(this, this, this);
        //init map
        mapPresenter.initMap(savedInstanceState);
    }


    @Override
    public void onMapReady(HuaweiMap map) {
        //get map instance in a callback method
        Log.d(TAG, "onMapReady: ");
        hMap = map;
        hMap.setMyLocationEnabled(false);
        hMap.getUiSettings().setMyLocationButtonEnabled(true);
        //get current location
        mapPresenter.initLocationKit();
        // set custom view to info window
        hMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(MainActivity.this));
        //move pin on clicked on map
        hMap.setOnMapClickListener(latLng -> mapPresenter.getLocationInformation("movePin", latLng));
        //on drag marker ( on start drag remove info window then on drag end view info window with new info position )
        hMap.setOnMarkerDragListener(new HuaweiMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                marker.hideInfoWindow();
            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                mapPresenter.getLocationInformation("movePin", marker.getPosition());
            }
        });
        // check if info window open or close and do the reverse
        hMap.setOnMarkerClickListener(marker -> {
            if (marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
            } else {
                marker.showInfoWindow();
            }
            return false;
        });
    }

    @Override
    public void showMap(Bundle mapViewBundle) {
        mMapView.onCreate(mapViewBundle);
        //get map instance
        mMapView.getMapAsync(this);
    }

    //update camera posation
    @Override
    public void changeCameraPosation(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition(latLng, 15.0f, 2.0f, 2.0f);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        hMap.animateCamera(cameraUpdate);
        hMap.moveCamera(cameraUpdate);
    }

    //on get current location or map click
    @Override
    public void LocationInfo(String type, LatLng latLng, AddressInfo addressInfo) {
        switch (type) {
            case "current":
                currentLocation = latLng;
                createStarMark(latLng, addressInfo);
                if (pinMarker == null) {
                    mapPresenter.showPin(latLng.latitude + 0.01, latLng.longitude + 0.01);
                }
                break;
            case "movePin":
                MovePinMark(latLng, addressInfo);
                break;
            case "showPin":
                showPinMark(latLng, addressInfo);
                break;
        }
    }

    //create star mark for current location
    private void createStarMark(LatLng latLng, AddressInfo addressInfo) {
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.star))
                .title(addressInfo.getFormatAddress())
                .snippet(addressInfo.getCountry());
        if (null != currentLocationMarker) {
            currentLocationMarker.remove();
        }
        currentLocationMarker = hMap.addMarker(options);
    }

    //moving pin when user drag or click
    private void MovePinMark(LatLng latLng, AddressInfo addressInfo) {
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin))
                .title(addressInfo.getFormatAddress())
                .snippet(addressInfo.getCountry())
                .draggable(true);
        if (null != pinMarker) {
            pinMarker.remove();
        }
        pinMarker = hMap.addMarker(options);
        pinMarker.showInfoWindow();
    }

    //show pin on create map
    private void showPinMark(LatLng latLng, AddressInfo addressInfo) {
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin))
                .title(addressInfo.getFormatAddress())
                .snippet(addressInfo.getCountry())
                .draggable(true);
        if (null != pinMarker) {
            pinMarker.remove();
        }
        pinMarker = hMap.addMarker(options);
    }


    //get info of current location
    @Override
    public void currentLocation(LatLng latLng) {
        mapPresenter.getLocationInformation("current", latLng);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapPresenter.removeLocationUpdatesWithCallback();
        mMapView.onDestroy();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

}
