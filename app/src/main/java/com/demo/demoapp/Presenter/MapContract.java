package com.demo.demoapp.Presenter;

import android.app.Activity;
import android.os.Bundle;

import com.demo.demoapp.Model.AddressInfo;
import com.huawei.hms.maps.MapView;
import com.huawei.hms.maps.model.LatLng;

import io.reactivex.Observable;

public class MapContract {
    public interface HandlingView {

        void showMap(Bundle mapViewBundle);

        void changeCameraPosation(LatLng latLng);

        void LocationInfo(String type, LatLng latLng, AddressInfo addressInfo);

        void currentLocation(LatLng latLng);
    }

    public interface HandlingPresenter {

        void initMap(Bundle savedInstanceState);

        void initLocationKit();

        void removeLocationUpdatesWithCallback();

        void getLocationInformation(String type, LatLng latLng);

        void showPin(double latitude, double longitude);
    }
}
