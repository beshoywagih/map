package com.demo.demoapp.Connections;

import com.demo.demoapp.Model.SiteKitResponse.SiteKitResponse;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Apis {

    @POST("mapApi/v1/siteService/reverseGeocode")
    Observable<Response<ResponseBody>> getLocationInfo(@Query(value = "key", encoded = true) String key,
                                                       @Body JsonObject siteKitRequest);
}
