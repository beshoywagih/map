package com.demo.demoapp.Model;

public class AddressInfo {
    String formatAddress;
    String country;

    public AddressInfo(String formatAddress, String country) {
        this.formatAddress = formatAddress;
        this.country = country;
    }

    public String getFormatAddress() {
        return formatAddress;
    }

    public void setFormatAddress(String formatAddress) {
        this.formatAddress = formatAddress;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
