package com.demo.demoapp.Model.SiteKitResponse;

public class Address {

    private String country;
    private String countrycode;
    private String sublocality;
    private String locality;
    private String adminarea;

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setSublocality(String sublocality) {
        this.sublocality = sublocality;
    }

    public String getSublocality() {
        return sublocality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getLocality() {
        return locality;
    }

    public void setAdminarea(String adminarea) {
        this.adminarea = adminarea;
    }

    public String getAdminarea() {
        return adminarea;
    }

}