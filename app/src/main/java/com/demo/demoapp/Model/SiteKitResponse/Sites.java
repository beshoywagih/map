package com.demo.demoapp.Model.SiteKitResponse;

public class Sites {

    private String formataddress;
    private Address address;
    private Viewport viewport;
    private String name;
    private String siteid;
    private Location location;

    public void setFormataddress(String formataddress) {
        this.formataddress = formataddress;
    }

    public String getFormataddress() {
        return formataddress;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

}