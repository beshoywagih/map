package com.demo.demoapp.Model.SiteKitResponse;

import java.util.List;

public class SiteKitResponse {

    private String returncode;
    private List<Sites> sites;
    private String returndesc;

    public void setReturncode(String returncode) {
        this.returncode = returncode;
    }

    public String getReturncode() {
        return returncode;
    }

    public void setSites(List<Sites> sites) {
        this.sites = sites;
    }

    public List<Sites> getSites() {
        return sites;
    }

    public void setReturndesc(String returndesc) {
        this.returndesc = returndesc;
    }

    public String getReturndesc() {
        return returndesc;
    }

}